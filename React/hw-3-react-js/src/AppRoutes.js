import React from 'react';
import { Routes, Route } from 'react-router-dom';
import HomePage from './Pages/Home/Home';
import FavouritesPage from './Pages/Favourites/Favourites';
import CartPage from './Pages/cartPage/CartPage';

const AppRoutes = ({ cards, setModalActive, changeIsFavourite, cartCards, setModalDeleteActive }) => {

    return (
        <Routes>
            <Route path='/' element={<HomePage cards={cards} setModalActive={setModalActive} changeIsFavourite={changeIsFavourite} cartCards={cartCards} />} />
            <Route path='/favourites' element={<FavouritesPage cards={cards} setModalActive={setModalActive} changeIsFavourite={changeIsFavourite} />} />
            <Route path='/cart' element={<CartPage cartCards={cartCards} setModalDeleteActive={setModalDeleteActive}/>} />
        </Routes>
    )
}

export default AppRoutes;