import React from 'react';
import CardsContainer from '../../Components/CardsContainer/CardsContainer';
import PropTypes from "prop-types";



const FavouritesPage = ({ cards, setModalActive, changeIsFavourite }) => {

    const favs = cards.filter(card => card.isFavourite)

    return (
        <>

            <div>
                <CardsContainer
                    cards={favs}
                    setModalActive={setModalActive}                   
                    changeIsFavourite={changeIsFavourite}
                />
            </div>

           
        </>
    )
}

FavouritesPage.propTypes = {
    cards: PropTypes.array,
    setModalActive: PropTypes.func, 
    changeIsFavourite: PropTypes.func
  }

FavouritesPage.defaultProps = {
    cards: [],
    setModalActive: ()=>{},
    changeIsFavourite: ()=>{}
}

export default FavouritesPage;