import React from 'react';
import CartCardsContainer from '../../Components/CartCardsContainer/CartCardsContainer'




const CartPage = ({ cartCards, setModalDeleteActive }) => {



    return (
        <>
            <div>
                <CartCardsContainer
                    cartCards={cartCards}
                    setModalDeleteActive={setModalDeleteActive}
                />
            </div>

        </>
    )
}

export default CartPage;