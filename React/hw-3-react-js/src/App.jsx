import styles from './App.module.scss';
import { useState, useEffect } from 'react';
import axios from 'axios';
import Button from "./Components/Button/Button";
import Modal from "./Components/Modal/Modal";
import Header from "./Components/Header/Header";
import { getStateFromLocalStorage } from './Components/utils/localStorageFunctions';
import { CART_LS_KEY } from './variables';
import { FAVOURITE_LS_KEY } from './variables';
import { saveStateToLocalStorage } from './Components/utils/localStorageFunctions';
import AppRoutes from './AppRoutes'

const bodyModal = `Ви впевнені, що хочете додати товар у кошик?`
const bodyModalAtCart = 'Ви впевнені, що хочете видалити товар з кошика?'

function App() {
  const [cards, setCards] = useState([]);
  const [cartCards, setCartCards] = useState([]);
  const [modalActive, setModalActive] = useState({ boolean: false, productId: null });
  const [modalDeleteActive, setModalDeleteActive] = useState({ boolean: false, productId: null });

  const getCards = async () => {
    try {
      const { data } = await axios('./fishingCards.json')

      const favFromLs = getStateFromLocalStorage(FAVOURITE_LS_KEY);

      if (favFromLs) {
        const favId = favFromLs.map(({ id }) => id);
        const cards = data.map((card) => ({ ...card, isFavourite: favId.includes(card.id) }))
        setCards(cards)
      }
      else { setCards(data) };

    } catch (err) {
      console.log(err)
    }
  }

  useEffect(() => {
    getCards();

    const cartFromLs = getStateFromLocalStorage(CART_LS_KEY);

    if (cartFromLs) {
      setCartCards(cartFromLs);
    }
  }, [])


  const addToCart = () => {
    setCartCards((prev) => {
      const newCartState = [...prev];

      const index = newCartState.findIndex((card) => card.id === modalActive.productId)
      if (index !== -1) {
        newCartState[index].count++;

        saveStateToLocalStorage(CART_LS_KEY, newCartState);


        return newCartState;
      } else {
        const { name, price, id, image } = cards.find((el) => el.id === modalActive.productId)

        const newState = [{ name, price, id, image, count: 1 }, ...prev];

        saveStateToLocalStorage(CART_LS_KEY, newState);

        return newState;
      }

    })
    setModalActive({ boolean: false, productId: null });
  }

  const deleteFromCart = () => {

    setCartCards((prev) => {
      const newCartState = [...prev];

      const newState = newCartState.filter(card => card.id !== modalDeleteActive.productId)

      saveStateToLocalStorage(CART_LS_KEY, newState);

      return newState;

    })
    setModalDeleteActive({ boolean: false, productId: null });
  }



  const changeIsFavourite = (id) => {

    setCards((prev) => {
      const newCardsState = [...prev];

      const index = newCardsState.findIndex((card) => card.id === id)

      if (!newCardsState[index].isFavourite) {

        newCardsState[index].isFavourite = true;

        const favCards = newCardsState.filter(card => card.isFavourite === true)
        saveStateToLocalStorage(FAVOURITE_LS_KEY, favCards);

        return newCardsState;

      }
      else {
        newCardsState[index].isFavourite = false;

        const favCards = newCardsState.filter(card => card.isFavourite === true)
        saveStateToLocalStorage(FAVOURITE_LS_KEY, favCards);

        return newCardsState;
      }
    })
  }

  return (
    <div className="App">
      <div className={styles.root}>
        <Header cartCards={cartCards} cards={cards} />

        <div className={styles.content}>

          <AppRoutes
            cards={cards}
            setModalActive={setModalActive}
            changeIsFavourite={changeIsFavourite}
            cartCards={cartCards}
            deleteFromCart={deleteFromCart}
            setModalDeleteActive={setModalDeleteActive}
          />
        </div>

        <Modal
          closeButton={true}
          backgroundColor={'lightgreen'}
          backgroundColorHeader={'green'}
          active={modalActive.boolean}
          setActive={setModalActive}
          bodyModal={bodyModal}
          text='Додати товар у кошик?'
          actions={
            <>
              <Button backgroundColor={'lightgrey'} onClick={addToCart} text='Ok' />
              <Button backgroundColor={'violet'} onClick={() => setModalActive({ boolean: false, productId: null })} text='Censel' />
            </>
          }
        />


        <Modal
          closeButton={false}
          backgroundColor={'red'}
          backgroundColorHeader={'red'}
          active={modalDeleteActive.boolean}
          setActive={setModalDeleteActive}
          bodyModal={bodyModalAtCart}
          text='Видалити товар з кошика?'
          actions={
            <>
              <Button backgroundColor={'lightgrey'} onClick={deleteFromCart} text='Ok' />
              <Button backgroundColor={'violet'} onClick={() => setModalDeleteActive({ boolean: false, productId: null })} text='Censel' />
            </>
          }
        />
      </div>
    </div>
  );
}


export default App;
