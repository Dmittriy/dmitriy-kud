import React from 'react';
import styles from './Cart.module.scss';
import CartCardAtHomePage from '../CartCardAtHomePage/CartCardAtHomePage';
import PropTypes from "prop-types";

const Cart = ({ cartCards }) => {
    let cartSumm = 0;
    cartCards.forEach(card => {
        cartSumm = cartSumm + card.price * card.count
    });

    return (
        <section className={styles.root}>
            <h2>ТАВАРИ У КОШИКУ</h2>
            <div className={styles.cards}>
                {cartCards && cartCards.map(({ name, count, id}) => <CartCardAtHomePage key={id} name={name} count={count}/>)}
            </div>
            <p>ВСЬОГО НА: {cartSumm}$</p>
        </section>
    )
}

Cart.propTypes = {
    cartCards: PropTypes.array
}

Cart.defaultProps = {
    cartCards: []
}


export default Cart;