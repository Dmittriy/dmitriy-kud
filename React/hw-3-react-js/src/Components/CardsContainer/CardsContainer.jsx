import React from 'react';
import styles from './CardsContainer.module.scss';
import Card from "../Card/Card";
import PropTypes from "prop-types";

const CardsContainer = ({ cards, setModalActive, changeIsFavourite }) => {



    return (
        <section className={styles.root}>
            <h1>КАТАЛОГ ТАВАРІВ</h1>
            <div className={styles.container}>
                {cards && cards.map(({ name, price, image, id, isFavourite }) =>
                    <Card
                        isFavourite={isFavourite}
                        key={id}
                        name={name}
                        price={price}
                        image={image}
                        id={id}                        
                        setModalActive={setModalActive}
                        changeIsFavourite={changeIsFavourite}
                    />)}
            </div>
        </section>
    )
}

CardsContainer.propTypes = {
    cards: PropTypes.array,
    setModalActive: PropTypes.func, 
    changeIsFavourite: PropTypes.func
  }

CardsContainer.defaultProps = {
    cards: [],
    setModalActive: () => { },
    changeIsFavourite: () => { }
}

export default CardsContainer;