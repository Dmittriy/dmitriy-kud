import React from 'react';
import styles from './Card.module.scss';
import PropTypes from "prop-types";
import Button from '../Button/Button';
import classNames from 'classnames';
import { ReactComponent as StarIconPlus } from '../../assets/svg/star-plus.svg'
import { ReactComponent as StarIconRemove } from '../../assets/svg/star-remove.svg'

const Card = ({ name, price, image, id, isFavourite, setModalActive, changeIsFavourite }) => {

    
    return (
        <div className={styles.root}>
            <div onClick={() => changeIsFavourite(id)} className={classNames(styles.favourites, { [styles.remove]: isFavourite })}>
                {isFavourite ? <StarIconRemove /> : <StarIconPlus />}

            </div>
            <p>{name}</p>
            <img src={image} alt={name} />
            <span>Вартість за одиницю : {price}$</span>
            <Button backgroundColor={'aquamarine'} onClick={() => { setModalActive({boolean:true, productId:id}) }} text='Додати в кошик' />
                      

        </div>
    )
}

Card.propTypes = {
    name: PropTypes.string,
    price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    image: PropTypes.string,
    setCartCards: PropTypes.func,
    isFavourite: PropTypes.bool,    
    setModalActive: PropTypes.func,
    changeIsFavourite: PropTypes.func
}

Card.defaultProps = {
    name: '',
    price: '',
    image: '',
    setCartCards: ()=>{},
    isFavourite: false,    
    setModalActive: ()=>{},
    changeIsFavourite: ()=>{}
}

export default Card;