import React from 'react';
import styles from './Modal.module.scss';
import Button from '../Button/Button';
import PropTypes from 'prop-types';

const Modal = ({ active, setActive, closeButton, text, backgroundColor, backgroundColorHeader, bodyModal, actions}) => {

    return (

        <div className={active ? [styles.wrapperModal, styles.active].join(' ') : styles.wrapperModal} onClick={() => setActive(false)} >
            <div className={styles.modal} onClick={(e) => e.stopPropagation()}>
                <div className={styles.header} style={{ background: backgroundColorHeader }}>
                    <h2 className={styles.title}>{text}</h2>
                    {closeButton &&
                        (<Button backgroundColor={'pink'} className={styles.closeBtn}
                            onClick={() => setActive({boolean: false, productId: null})} text='X' />
                        )}
                </div>
                <div className={styles.conteiner} style={{ backgroundColor }}>
                    <p>{bodyModal}</p>

                    <div className={styles.btnWrapper}>

                        {actions}
                        
                    </div>
                </div>
            </div>
        </div>
    )
}

Modal.propTypes = {
    active : PropTypes.bool.isRequired,
    setActive : PropTypes.func.isRequired ,
    closeButton : PropTypes.bool,
    text : PropTypes.string,
    backgroundColor : PropTypes.string,
    backgroundColorHeader : PropTypes.string,
    bodyModal : PropTypes.string,
    actions : PropTypes.object       
  }

Modal.defaultProps = {
    closeButton : true,
    text : " ",
    active : false,
    setActive: ()=>{},
    backgroundColor : "white",
    backgroundColorHeader : "white",
    bodyModal : " ",
    actions : " " 
}


export default Modal;




