import React from 'react';
import styles from './CartItem.module.scss';
import PropTypes from "prop-types";
import Button from '../Button/Button';
// import Modal from '../Modal/Modal';
// import { useState } from 'react';
// import classNames from 'classnames';

// const bodyModalAtCart = 'Ви впевнені, що хочете видалити товар з кошика?'

const CartItem = ({ name, price, image, id, count, setModalDeleteActive }) => {

    // const [modalCartActive, setModalCartActive] = useState({ boolean: false, productId: null });


    return (
        <div className={styles.root}>
            <p>{name}</p>
            <img src={image} alt={name} />
            <span>Кількість : {count}</span>
            <span>Вартість за одиницю : {price}$</span>
            <Button backgroundColor={'aquamarine'} onClick={() => { setModalDeleteActive({ boolean: true, productId: id }) }} text='Видалити з кошика' />



        </div>
    )
}

CartItem.propTypes = {
    name: PropTypes.string,
    price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    image: PropTypes.string,
    setCartCards: PropTypes.func,
    setModalDeleteActive: PropTypes.func,
    count: PropTypes.number,
    deleteFromCart: PropTypes.func
}

CartItem.defaultProps = {
    name: '',
    price: '',
    image: '',
    count: '',
    setCartCards: () => { },
    setModalDeleteActive: () => { },
    deleteFromCart: () => { }

}

export default CartItem;