import React from 'react';
import styles from './CartCardsContainer.module.scss';
import CartItem from '../CartItem/CartItem'
import PropTypes from "prop-types";

const CartCardsContainer = ({ cartCards, setModalDeleteActive }) => {



    return (
        <section className={styles.root}>
            <h1>ТАВАРИ У КОШИКУ</h1>
            <div className={styles.container}>
                {cartCards && cartCards.map(({ name, price, image, id, count }) =>
                    <CartItem
                        count={count}
                        key={id}
                        name={name}
                        price={price}
                        image={image}
                        id={id}
                        setModalDeleteActive={setModalDeleteActive}
                    />)}
            </div>
        </section>
    )
}

CartCardsContainer.propTypes = {
    cartCards: PropTypes.array,
    setModalActive: PropTypes.func
}

CartCardsContainer.defaultProps = {
    cartCards: [],
    setModalActive: () => { }
}

export default CartCardsContainer;