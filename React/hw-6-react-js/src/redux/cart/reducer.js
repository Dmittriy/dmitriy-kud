import { SET_CART, ADD_TO_CART, DELETE_FROM_CART } from "./actions";
import { saveStateToLocalStorage } from "../../Components/utils/localStorageFunctions";
import { CART_LS_KEY } from "../../variables";
const initialValue = {
  cards: [],
  count: 0,
  summPrice: 0,
};

const cartReducer = (state = initialValue, action) => {
  switch (action.type) {
    case SET_CART: {
      const cards = action.payload;
      let count = 0;
      let summPrice = 0;
      cards.forEach((card) => {
        count += card.count;
        summPrice += card.price;
      });

      return { ...state, cards, count, summPrice };
    }
    case ADD_TO_CART: {
      let newCards = [...state.cards];
      let newCount = state.count;
      let newSummPrice = state.summPrice;
      const card = action.payload;
      const index = newCards.findIndex((el) => el.id === card.id);
      if (index !== -1) {
        newCards[index].count++;
        newCount++;
        newSummPrice += card.price;

        saveStateToLocalStorage(CART_LS_KEY, newCards);
        return {
          ...state,
          cards: newCards,
          count: newCount,
          summPrice: newSummPrice,
        };
      } else {
        card.count = 1;
        newCount++;
        newSummPrice += card.price;
        newCards.push(card);
        saveStateToLocalStorage(CART_LS_KEY, newCards);
        return {
          ...state,
          cards: newCards,
          count: newCount,
          summPrice: newSummPrice,
        };
      }
    }

    case DELETE_FROM_CART: {
      const newCards = [...state.cards];
      let newCount = state.count;
      let newSummPrice = state.summPrice;
      const card = action.payload;
      const index = newCards.findIndex((el) => el.id === card.id);
      newCards.splice(index, 1);
      newSummPrice -= card.price * card.count;
      newCount -= card.count;
      saveStateToLocalStorage(CART_LS_KEY, newCards);
      return {
        ...state,
        cards: newCards,
        count: newCount,
        summPrice: newSummPrice,
      };
    }

    default:
      return state;
  }
};

export default cartReducer;
