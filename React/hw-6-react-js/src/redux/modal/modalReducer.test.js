import modalReducer from "./reducer";
import { setModal, setModalTitle, setModalBody,setModalAction } from "./actionCreators";

const initialValue = {
  isActive: false,
  modalAction: null,
  title: "",
  body: "",
};

describe("Modal reducer tests", () => {
  test("should modalReducer return default value without state and action", () => {
    expect(modalReducer(undefined, { type: undefined })).toEqual(initialValue);
  });

  test("should modalReducer change isActive value", () => {
    expect(modalReducer(undefined, setModal(true))).toEqual({
      isActive: true,
      modalAction: null,
      title: "",
      body: "",
    });
  });

  test("should modalReducer change title value", () => {
    expect(
      modalReducer(
        undefined,
        setModalTitle("TEST TEST!!!")
      )
    ).toEqual({
      isActive: false,
      modalAction: null,
      title: "TEST TEST!!!",
      body: "",
    });
  });
  test("should modalReducer change body value", () => {
    expect(
      modalReducer(
        undefined,
        setModalBody("TESTING MODAL")
      )
    ).toEqual({
      isActive: false,
      modalAction: null,
      title: "",
      body: "TESTING MODAL",
    });
  });
  test("should modalReducer change madalAction value", () => {
    expect(
      modalReducer(
        undefined,
        setModalAction(<button>Confirm</button>)
      )
    ).toEqual({
      isActive: false,
      modalAction: <button>Confirm</button>,
      title: "",
      body: "",
    });
  });
});
