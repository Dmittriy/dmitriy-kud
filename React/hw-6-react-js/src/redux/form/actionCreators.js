import { SET_FORM} from "./actions";

export const setForm = () => ({
  type: SET_FORM,
  payload: '',
});
