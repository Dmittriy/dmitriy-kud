import React from "react";
import styles from "./CartCardAtHomePage.module.scss";
import PropTypes from "prop-types";

const CartCardAtHomePage = ({ name, count }) => {
  return (
    <div className={styles.root}>
      <div>
        <span>{name}</span>
      </div>
      <span>{count}</span>
    </div>
  );
};

CartCardAtHomePage.propTypes = {
  name: PropTypes.string,
  count: PropTypes.number,
};

CartCardAtHomePage.defaultProps = {
  name: "",
  count: "",
};

export default CartCardAtHomePage;
