import { render } from "@testing-library/react";
import Card from './Card';
import { Provider} from "react-redux";
import store from "../../redux/store";



const Component = (props) => {
  return (
    <Provider store={store}>
      <Card/>
    </Provider>
  );
};

describe("Snapshot test", () => {
  test("should Card render", () => {
    const { asFragment } = render(<Component />);

    expect(asFragment()).toMatchSnapshot();
  });
});
