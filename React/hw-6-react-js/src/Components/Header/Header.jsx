import React, { memo } from "react";
import styles from "./Header.module.scss";
import { ReactComponent as BasketIcon } from "../../assets/svg/basket.svg";
import { ReactComponent as FavoritetIcon } from "../../assets/svg/favorite.svg";
import { NavLink } from "react-router-dom";
import classNames from "classnames";
import { useSelector } from "react-redux";
import Button from "../Button/Button";
import { useContext } from "react";
import ViewContext from "../../context";


const Header = () => {
  const fav = useSelector((state) => state.favourite.cards.length);
  const count = useSelector((state) => state.cart.count);
  const { toggleView } = useContext(ViewContext);
  // const isActiceCart =

  return (
    <header className={styles.header}>
      <span className={styles.logo}>Fishing Shop</span>

      <nav>
        <ul style={{ display: "flex", gap: 100 }}>
          <li>
            <NavLink
              className={({ isActive }) =>
                `${styles.link} ${isActive ? styles.active : ""}`
              }
              to="/"
            >
              Home
            </NavLink>
          </li>

          <li>
            <NavLink
              className={({ isActive }) =>
                `${styles.link} ${isActive ? styles.active : ""}`
              }
              to="/favourites"
            >
              Favourites
            </NavLink>
          </li>

          <li>
            <NavLink id='header_cart'
              className={({ isActive }) =>
                classNames(styles.link, { [styles.active]: isActive })
              }
              to="/cart"
            >
              Cart
            </NavLink>
          </li>
        </ul>
      </nav>

      <div className={styles.iconWrapper}>
        <div className={styles.iconContainer}>
          <FavoritetIcon />
          <p className={styles.iconNumberFavourite}>{fav}</p>
        </div>

        <div className={styles.iconContainer}>
          <BasketIcon />
          <p className={styles.iconNumber}>{count}</p>
        </div>
      </div>

      {/* <Button onClick={toggleView} text="CHANGE VIEW" backgroundColor={"rgb(116, 232, 98)"}></Button> */}
      <button className={styles.buttonChange} onClick={toggleView} >CHANGE VIEW</button>
    </header>
  );
};

export default memo(Header);
