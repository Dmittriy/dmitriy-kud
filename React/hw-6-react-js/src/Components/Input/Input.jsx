import React from 'react';
import { useField } from 'formik';
import styles from "./Input.module.scss";

const Input = (props) =>{
    const [field, meta] = useField(props.name);
    const{error, touched}= meta;
  return (
    <div>
        <input className={styles.input} type="text" {...field} {...props} />
        {error && touched ? <p className={styles.error}>{error}</p> : null}
    </div>
  )
}

export default Input;
