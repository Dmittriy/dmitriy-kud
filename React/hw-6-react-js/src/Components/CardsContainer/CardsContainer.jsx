import React from "react";
import styles from "./CardsContainer.module.scss";
import Card from "../Card/Card";
import { useContext } from "react";
import ViewContext from "../../context";
import classNames from "classnames";

const CardsContainer = ({ cards }) => {
  const { view } = useContext(ViewContext);
  return (
    <section className={styles.root}>
      <h1>КАТАЛОГ ТАВАРІВ</h1>
      <div
        className={classNames({
          [styles.containerViewCards]: view === "viewCards",
          [styles.containerViewLists]: view === "viewList",
        })}
      >
        {cards &&
          cards.map(({ name, price, image, id, isFavourite }) => (
            <Card
              isFavourite={isFavourite}
              key={id}
              name={name}
              price={price}
              image={image}
              id={id}
            />
          ))}
      </div>
    </section>
  );
};

export default CardsContainer;
