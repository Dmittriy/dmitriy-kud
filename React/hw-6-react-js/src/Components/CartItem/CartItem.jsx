import React from "react";
import styles from "./CartItem.module.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import { useDispatch } from "react-redux";
import { setModal, setModalAction, setModalBody, setModalTitle } from "../../redux/modal/actionCreators";
import { deleteFromCart } from "../../redux/cart/actionCreators";

const bodyModalAtCart = "Ви впевнені, що хочете видалити товар з кошика?";
const CartItem = ({ name, price, image, id, count }) => {
  const dispatch = useDispatch();
  const toggleModal = () => {
    dispatch(setModal());
  };

  const handleDeleteFromCart = () => {
    dispatch(
      setModalAction(
        <Button
          backgroundColor={"lightgrey"}
          onClick={() => {
            dispatch(deleteFromCart({ id, count, price }));
            toggleModal();
          }}
          text="Ok"
        />
      )
    );
    
    dispatch(setModalTitle("Видалити товар з кошика?"))
    dispatch(setModalBody(bodyModalAtCart))
    toggleModal();
  };

  return (
    <div className={styles.root}>
      <p>{name}</p>
      <img src={image} alt={name} />
      <span>Кількість : {count}</span>
      <span>Вартість за одиницю : {price}$</span>
      <Button
        backgroundColor={"aquamarine"}
        onClick={handleDeleteFromCart}
        text="Видалити з кошика"
      />
    </div>
  );
};

CartItem.propTypes = {
  name: PropTypes.string,
  price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  image: PropTypes.string,
  count: PropTypes.number,
};

CartItem.defaultProps = {
  name: "",
  price: "",
  image: "",
  count: "",
};

export default CartItem;
