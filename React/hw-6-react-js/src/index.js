import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import { ErrorBoundary } from "react-error-boundary";
import { Provider } from "react-redux";
import store from "./redux/store";
import ViewContextProvider from "./context/ViewContextProvider";


const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
    <ErrorBoundary>
      <BrowserRouter>
        <ViewContextProvider>
          <App />
        </ViewContextProvider>
      </BrowserRouter>
    </ErrorBoundary>
  </Provider>
);
