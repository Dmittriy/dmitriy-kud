import React, { useState } from 'react'
import ViewContext from '.'

function ViewContextProvider({ children }) {

    const [view, setView] = useState('viewCards');

    const toggleView = () => {

        setView(prev => prev === 'viewCards' ? 'viewList' : 'viewCards');
    }

    const value = {
        view,
        toggleView,
    };

    return (
        <ViewContext.Provider value={value}>
            {children}
        </ViewContext.Provider>
    )
}

export default ViewContextProvider