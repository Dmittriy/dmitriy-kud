import styles from './App.module.scss';
import { useState, useEffect } from 'react';
import axios from 'axios';
// import Button from "./Components/Button/Button";
// import Modal from "./Components/Modal/Modal";
import Header from "./Components/Header/Header";
import CardsContainer from "./Components/CardsContainer/CardsContainer";
import { getStateFromLocalStorage } from './Components/utils/localStorageFunctions';
import { CART_LS_KEY } from './variables';
import Cart from './Components/Cart/Cart';


// const bodyModal = `Ви впевнені, що хочете додати товар у кошик?
// `

function App() {
  const [cards, setCards] = useState([]);
  const [cartCards, setCartCards] = useState([]);
  const [modalActive, setModalActive] = useState(false);

  const getCards = async () => {
    try {
      const { data } = await axios('./fishingCards.json')

      setCards(data);

    } catch (err) {
      console.log(err)
    }
  }

  useEffect(() => {
    getCards();

    const cartFromLs = getStateFromLocalStorage(CART_LS_KEY);

    if (cartFromLs) {
      setCartCards(cartFromLs);
    }
  }, [])

   return (
    <div className="App">
      <div className={styles.root}>
        <Header cartCards={cartCards} />
        <div className={styles.content}>
          <div>
            <CardsContainer 
            cards={cards} 
            setCards= {setCards}
            setCartCards={setCartCards} 
            setModalActive={setModalActive} 
            modalActive = {modalActive}
            />
          </div>

          <div>
            <Cart cards={cartCards} />
          </div>
        </div>

      </div>
      
            
    </div>
  );
}


export default App;
