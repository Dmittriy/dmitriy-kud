import React from 'react';
import styles from './Button.module.scss';
import PropTypes from 'prop-types';

const Button = ({ backgroundColor, text, onClick }) => {

  return (
    <>
      <button className={styles.button} style={{ backgroundColor }} onClick={onClick} >{text}</button>
    </>

  )
}

Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func
}

Button.defaultProps = {
  backgroundColor: "white",
  text: " ",
  onClick : ()=>{}
}


export default Button;