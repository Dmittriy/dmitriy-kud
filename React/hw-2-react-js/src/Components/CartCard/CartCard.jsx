import React from 'react';
import styles from './CartCard.module.scss';

const CartCard = ({ name, count }) => {
    
    return (
        <div className={styles.root}>
            <div>
                <span>{name}</span>
            </div>
            <span>{count}</span>
        </div>
    )
}

export default CartCard;