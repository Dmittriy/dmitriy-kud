import React from 'react';
import styles from './CardsContainer.module.scss';
import Card from "../Card/Card";
import PropTypes from "prop-types";
import { saveStateToLocalStorage } from '../utils/localStorageFunctions';
import { CART_LS_KEY } from '../../variables';

const CardsContainer = ({ cards, setCards, setCartCards, modalActive, setModalActive }) => {

    const changeIsFavourite = async (id) => {

        setCards((prev) => {
            const newCardsState = [...prev];

            const index = newCardsState.findIndex((card) => card.id === id)

            if (newCardsState[index].isFavourite === false) {

                newCardsState[index].isFavourite = true;
                
                return newCardsState;

            }
            else {
                newCardsState[index].isFavourite = false;
                
                return newCardsState;
            }
        })


    }


    return (
        <section className={styles.root}>
            <h1>КАТАЛОГ ТАВАРІВ</h1>
            <div className={styles.container}>
                {cards && cards.map(({ name, price, image, id, isFavourite }) =>
                    <Card

                        isFavourite={isFavourite}
                        key={id}
                        name={name}
                        price={price}
                        image={image}
                        id={id}
                        setCards={setCards}
                        setCartCards={setCartCards}
                        modalActive={modalActive}
                        setModalActive={setModalActive}
                        changeIsFavourite={changeIsFavourite}

                    />)}
            </div>
        </section>
    )
}

CardsContainer.propTypes = {
    cards: PropTypes.array,
    setCartCards: PropTypes.func
}

CardsContainer.defaultProps = {
    cards: [],
    setCartCards: () => { }
}

export default CardsContainer;