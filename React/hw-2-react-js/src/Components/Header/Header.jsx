import React, { memo } from 'react';
import styles from './Header.module.scss';

const Header = ({ cartCards }) => {
  console.log('Header');

  let counter = 0;
  cartCards.forEach(({ count }) => {
    counter += count;
  })

 
  return (
    <header className={styles.header}>
      <span className={styles.logo}>Fishing Shop</span>
      <div className={styles.cardContainer}>
        КОШИК
        <p className={styles.cardNumber}>
          {counter}
          </p>
      </div>
    </header>
  )
}

export default memo(Header);