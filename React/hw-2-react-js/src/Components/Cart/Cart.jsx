import React from 'react';
import styles from './Cart.module.scss';
import CartCard from '../CartCard/CartCard';

const Cart = ({cards}) => {
    return (
        <section className={styles.root}>
            <h2>ТАВАРИ У КОШИКУ</h2>
            <div className={styles.cards}>
                {cards && cards.map(({ name, count, id }) => <CartCard key={id} name={name} count={count} />)}
            </div>
            <p>ВСЬОГО НА: 0$</p>
        </section>
    )
}

export default Cart;