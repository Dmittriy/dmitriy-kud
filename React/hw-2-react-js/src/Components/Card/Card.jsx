import React from 'react';
import styles from './Card.module.scss';
import PropTypes from "prop-types";
import { saveStateToLocalStorage } from '../utils/localStorageFunctions';
import { CART_LS_KEY } from '../../variables';
import Button from '../Button/Button';
import Modal from '../Modal/Modal';

import classNames from 'classnames';

import { ReactComponent as StarIconPlus } from '../../assets/svg/star-plus.svg'
import { ReactComponent as StarIconRemove } from '../../assets/svg/star-remove.svg'

const bodyModal = `Ви впевнені, що хочете додати товар у кошик?`

const Card = ({ name, price, image, id, setCards, setCartCards, isFavourite, modalActive, setModalActive, changeIsFavourite }) => {

    const addToCart = () => {
        setCartCards((prev) => {
            const newCartState = [...prev];

            const index = newCartState.findIndex((card) => card.id === id)

            if (index !== -1) {
                newCartState[index].count++;

                saveStateToLocalStorage(CART_LS_KEY, newCartState);

                return newCartState;
            } else {
                const newState = [{ name, price, id, count: 1 }, ...prev];
                saveStateToLocalStorage(CART_LS_KEY, newState);

                return newState;
            }

        })
        setModalActive(false);
    }



    return (
        <div className={styles.root}>
            <div onClick={() => changeIsFavourite(id)} className={classNames(styles.favourites, { [styles.remove]: isFavourite })}>
                {isFavourite ? <StarIconRemove /> : <StarIconPlus />}

            </div>
            <p>{name}</p>
            <img src={image} alt={name} />
            <span>{price}$</span>
            <Button backgroundColor={'aquamarine'} onClick={() => { setModalActive(true) }} text='Додати в кошик' />
            <button onClick={addToCart}>Додати в кошик</button>


            <Modal closeButton={true} backgroundColor={'lightgreen'} backgroundColorHeader={'green'} active={modalActive}

                actions={
                    <>
                        <Button backgroundColor={'lightgrey'} onClick={addToCart} text='Ok' />
                        <Button backgroundColor={'violet'} onClick={() => setModalActive(false)} text='Censel' />
                    </>
                }
                setActive={setModalActive} bodyModal={bodyModal} text='Додати товар у кошик?' />

        </div>
    )
}

Card.propTypes = {
    name: PropTypes.string,
    price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    image: PropTypes.string,
    setCartCards: PropTypes.func,
    isFavourite: PropTypes.bool,
    modalActive: PropTypes.bool,
    setModalActive: PropTypes.func,
    changeIsFavourite: PropTypes.func
}

Card.defaultProps = {
    name: '',
    price: '',
    image: '',
    setCartCards: () => { },
    isFavourite: false,
    modalActive: false,
    setModalActive: () => { },
    // changeIsFavourite: ()=>{}


}

export default Card;