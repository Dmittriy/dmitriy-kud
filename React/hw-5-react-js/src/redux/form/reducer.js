import { SET_FORM } from "./actions";
import { saveStateToLocalStorage } from "../../Components/utils/localStorageFunctions";
import { CART_LS_KEY } from "../../variables";
const initialValue = {
  isBued: "",
};

const formReducer = (state = initialValue, action) => {
  switch (action.type) {
    case SET_FORM: {
      saveStateToLocalStorage(CART_LS_KEY, []);

      return { ...state, isBued: action.payload };
    }

    default:
      return state;
  }
};

export default formReducer;
