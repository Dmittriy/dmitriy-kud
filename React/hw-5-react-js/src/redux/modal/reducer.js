import {
  SET_MODAL,
  SET_MODAL_ACTION,
  SET_MODAL_TITLE,
  SET_MODAL_BODY,
} from "./actions";

const initialValue = {
  isActive: false,
  modalAction: null,
  title: "",
  body: "",
};

const modalReducer = (state = initialValue, action) => {
  switch (action.type) {
    case SET_MODAL: {
      return { ...state, isActive: !state.isActive };
    }
    case SET_MODAL_ACTION: {
      return { ...state, modalAction: action.payload };
    }
    case SET_MODAL_TITLE: {
      return { ...state, title: action.payload };
    }
    case SET_MODAL_BODY: {
      return { ...state, body: action.payload };
    }

    default:
      return state;
  }
};

export default modalReducer;
