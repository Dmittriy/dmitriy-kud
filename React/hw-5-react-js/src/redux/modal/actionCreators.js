import { SET_MODAL, SET_MODAL_ACTION, SET_MODAL_TITLE, SET_MODAL_BODY } from "./actions";

export const setModal = () => ({ type: SET_MODAL });

export const setModalAction = (value) => ({
  type: SET_MODAL_ACTION,
  payload: value,
});
export const setModalTitle = (value) => ({
  type: SET_MODAL_TITLE,
  payload: value,
});
export const setModalBody = (value) => ({
  type: SET_MODAL_BODY,
  payload: value,
});
