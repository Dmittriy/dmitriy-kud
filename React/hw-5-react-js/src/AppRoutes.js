import React from "react";
import { Routes, Route } from "react-router-dom";
import HomePage from "./Pages/Home/Home";
import FavouritesPage from "./Pages/Favourites/Favourites";
import CartPage from "./Pages/cartPage/CartPage";

const AppRoutes = () => {
  return (
    <Routes>
      <Route path="/" element={<HomePage />} />
      <Route path="/favourites" element={<FavouritesPage />} />
      <Route path="/cart" element={<CartPage />} />
      <Route path="*" element={<h1>404 - PAGE NO FOUND</h1>} />
    </Routes>
  );
};

export default AppRoutes;
