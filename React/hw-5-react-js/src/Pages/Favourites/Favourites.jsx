import React from "react";
import CardsContainer from "../../Components/CardsContainer/CardsContainer";
import PropTypes from "prop-types";
import { useSelector, shallowEqual } from "react-redux";

const FavouritesPage = () => {
  const cards = useSelector((state) => state.favourite.cards, shallowEqual);

  return (
    <>
      <div>
        <CardsContainer cards={cards} />
      </div>
    </>
  );
};

FavouritesPage.propTypes = {
  cards: PropTypes.array,
};

FavouritesPage.defaultProps = {
  cards: [],
};

export default FavouritesPage;
