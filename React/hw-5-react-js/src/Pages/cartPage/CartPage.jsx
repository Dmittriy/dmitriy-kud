import React from "react";
import CartCardsContainer from "../../Components/CartCardsContainer/CartCardsContainer";
import FormForBuy from "../../Components/FormForBuy/FormForBuy";
import { useSelector, shallowEqual } from "react-redux";
import styles from './CartPage.module.scss'
const CartPage = () => {
  const cartCards = useSelector(
    (state) => state.cart.cards.length,
    shallowEqual
  );
  return (
    <>
      <div className={styles.wrapper}   >
        <div>
          <CartCardsContainer />
        </div>

        {cartCards !== 0 && <FormForBuy />}
      </div>
    </>
  );
};

export default CartPage;
