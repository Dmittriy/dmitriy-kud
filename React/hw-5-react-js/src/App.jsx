import styles from "./App.module.scss";
import { useEffect } from "react";
import Modal from "./Components/Modal/Modal";
import Header from "./Components/Header/Header";
import AppRoutes from "./AppRoutes";
import { useDispatch, useSelector } from "react-redux";
import { axiosCards } from "./redux/cards/actionCreators";
import { axiosFavourite } from "./redux/favourite/actionCreators";
import { axiosCart } from "./redux/cart/actionCreators";

function App() {
  const isActiveModal = useSelector((state) => state.modal.isActive);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(axiosCards());
    dispatch(axiosFavourite());
    dispatch(axiosCart());
  }, []);

  return (
    <div className="App">
      <div className={styles.root}>
        <Header />

        <div className={styles.content}>
          <AppRoutes />
        </div>

        {isActiveModal && (
          <Modal
            closeButton={true}
            backgroundColor={"lightgreen"}
            backgroundColorHeader={"green"}
            active={isActiveModal}
          />
        )}
      </div>
    </div>
  );
}

export default App;
