import React from "react";
import { Formik, Form } from "formik";
import validationSchema from "./validationSchema";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { setCart } from "../../redux/cart/actionCreators";
import { setForm } from "../../redux/form/actionCreators";
import Input from "../Input/Input";
import styles from "./FormForBuy.module.scss";

const FormForBuy = () => {
  const dispatch = useDispatch();
  const initialValues = {
    name: "",
    lastName: "",
    age: "",
    addres: "",
    mobile: "",
  };
  const cartCards = useSelector((state) => state.cart.cards, shallowEqual);

  const submit = (value, { resetForm }) => {
    console.log({
      userInfo: value,
      cartInfo: cartCards,
    });
    dispatch(setForm());
    dispatch(setCart([]));
    resetForm();
  };

  return (
    <>
      <div className={styles.form}>
        <h1>Для оформлення замовлення, заповніть форму</h1>

        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={submit}
        >
          {({ isValid }) => {
            return (
              <Form
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  gap: "20px",
                  marginTop: "20px",
                }}
              >
                <Input placeholder="Ім'я" id="name" name="name" type="text" />
                <Input placeholder="Прізвище" name="lastName" type="text" />
                <Input placeholder="Вік" name="age" type="text" />
                <Input placeholder="Адреса" name="addres" type="text" />
                <Input
                  placeholder="Номер мобільного телефону"
                  name="mobile"
                  type="text"
                />

                <button
                  disabled={!isValid}
                  type="submit"
                  style={{
                    fontSize: "24px",
                    fontWeight: "bold",
                  }}
                >
                  Підтвердити замовлення
                </button>
              </Form>
            );
          }}
        </Formik>
      </div>
    </>
  );
};

export default FormForBuy;
