import { object, string, number } from "yup";

const nameRegExp = /[a-zA-zа-яА-яёЁ]$/;
const phoneRegExp = /[+1-9][0-9]{10}$/;

const validationSchema=object({ 
    name: string()
    .required('Обов\'язкове поле')
    .matches( nameRegExp, "Допускаються тільки латинські або кирилічні літери" )
    .min( 2, "В цьому полі має бути не менше 2-х символов" )
    .max( 15, "В цьому полі має бути не більше 15-ти символів" ),
    lastName: string()
    .required('Обов\'язкове поле' )
    .matches( nameRegExp, "Допускаються тільки латинські або кирилічні літери" )
    .min( 2, "В цьому полі має бути не менше 2-х символов" )
    .max( 15, "В цьому полі має бути не більше 15-ти символів" ),
    age: number()
    .required('Обов\'язкове поле')
    .min(16, 'Мінімум 16 років')
    .max(120, 'Максимум 120 років')
    .nullable(true, ),
    addres: string()
    .required( 'Обов\'язкове поле')
    .min( 2, "В цьому полі має бути не менше 2-х символів" )
    .max( 100, "В цьому полі має бути не більше 100-а символів" ),
    mobile: string()
    .required( 'Обов\'язкове поле')
    .matches(phoneRegExp, "Телефон має відповідати виду: +380671234567")
})

export default validationSchema;