import React from "react";
import styles from "./CardsContainer.module.scss";
import Card from "../Card/Card";

const CardsContainer = ({ cards }) => {
  return (
    <section className={styles.root}>
      <h1>КАТАЛОГ ТАВАРІВ</h1>
      <div className={styles.container}>
        {cards &&
          cards.map(({ name, price, image, id, isFavourite }) => (
            <Card
              isFavourite={isFavourite}
              key={id}
              name={name}
              price={price}
              image={image}
              id={id}
            />
          ))}
      </div>
    </section>
  );
};

export default CardsContainer;
