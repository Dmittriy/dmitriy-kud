import React from "react";
import styles from "./Cart.module.scss";
import CartCardAtHomePage from "../CartCardAtHomePage/CartCardAtHomePage";
import { useSelector } from "react-redux";

const Cart = () => {
  const cartCards = useSelector((state) => state.cart.cards);
  const fullPrice = useSelector((state) => state.cart.summPrice);

  return (
    <section className={styles.root}>
      <h2>ТАВАРИ У КОШИКУ</h2>
      <div className={styles.cards}>
        {cartCards &&
          cartCards.map(({ name, count, id }) => (
            <CartCardAtHomePage key={id} name={name} count={count} />
          ))}
      </div>
      <p>ВСЬОГО НА: {fullPrice}$</p>
    </section>
  );
};

export default Cart;
