import React from "react";
import styles from "./Card.module.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import classNames from "classnames";
import { ReactComponent as StarIconPlus } from "../../assets/svg/star-plus.svg";
import { ReactComponent as StarIconRemove } from "../../assets/svg/star-remove.svg";
import { useDispatch, useSelector } from "react-redux";
import { setModal, setModalBody, setModalTitle } from "../../redux/modal/actionCreators";
import { changeFavourite } from "../../redux/favourite/actionCreators";
import { addToCart } from "../../redux/cart/actionCreators";
import { setModalAction } from "../../redux/modal/actionCreators";

const bodyModal = `Ви впевнені, що хочете додати товар у кошик?`;

const Card = ({ name, price, image, id }) => {
  const dispatch = useDispatch();
  const toggleModal = () => {
    dispatch(setModal());
  };

  const handleAddToCart = () => {
    dispatch(
      setModalAction(
        <Button backgroundColor={"lightgrey"} onClick={() => {
          dispatch(addToCart({ name, price, image, id }))
          toggleModal();
        }} text="Ok" />
      )
    );
    dispatch(setModalTitle("Додати товар в кошик?"))
    dispatch(setModalBody(bodyModal))

    toggleModal();
  };

  const changeFavourites = () => {
    dispatch(changeFavourite({ name, price, image, id }));
  };
  const favourites = useSelector((state) => state.favourite.cards);
  const isFavourite = !!favourites.find((el) => el.id === id);

  return (
    <div className={styles.root}>
      <div
        onClick={changeFavourites}
        className={classNames(styles.favourites, {
          [styles.remove]: isFavourite,
        })}
      >
        {isFavourite ? <StarIconRemove /> : <StarIconPlus />}
      </div>
      <p>{name}</p>
      <img src={image} alt={name} />
      <span>Вартість за одиницю : {price}$</span>
      <Button
        backgroundColor={"aquamarine"}
        onClick={handleAddToCart}
        text="Додати в кошик"
      />
    </div>
  );
};

Card.propTypes = {
  name: PropTypes.string,
  price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  image: PropTypes.string,
  id: PropTypes.number,
};

Card.defaultProps = {
  name: "",
  price: "",
  image: "",
  id: null,
};

export default Card;
