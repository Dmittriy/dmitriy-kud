
export const saveStateToLocalStorage = (key, state) => {
    window.localStorage.setItem(key, JSON.stringify(state));
}


export const getStateFromLocalStorage = (key) => {
    const localStorageValue = window.localStorage.getItem(key);

    if (localStorageValue) {
        return JSON.parse(localStorageValue);
    } 

    return null;
}