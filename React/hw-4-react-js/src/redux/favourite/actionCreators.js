import { SET_FAVOURITE, CHANGE_FAVOURITE } from "./actions";
import { FAVOURITE_LS_KEY } from "../../variables";
import { getStateFromLocalStorage } from "../../Components/utils/localStorageFunctions";

export const setFavourite = (cards) => ({
  type: SET_FAVOURITE,
  payload: cards,
});

export const changeFavourite = (cards) => ({
  type: CHANGE_FAVOURITE,
  payload: cards,
});

export const axiosFavourite = () => {
  return async (dispatch) => {
    try {
      const favFromLs = getStateFromLocalStorage(FAVOURITE_LS_KEY);

      if (favFromLs) {
        dispatch(setFavourite(favFromLs));
      }

    } catch (error) {
      console.log("ERROR", error);
    }
  };
};
