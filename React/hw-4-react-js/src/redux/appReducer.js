import { combineReducers } from "redux";
import cardsReducer from "./cards/reducer";
import favouriteReducer from "../redux/favourite/reducer";
import modalReducer from "./modal/reducer";
import cartReducer from "./cart/reducer";

const appReducer = combineReducers({
  cards: cardsReducer,
  modal: modalReducer,
  favourite: favouriteReducer,
  cart:cartReducer

});

export default appReducer;
