import React from "react";
import CardsContainer from "../../Components/CardsContainer/CardsContainer";
import Cart from "../../Components/Cart/Cart";
import { useSelector, shallowEqual} from "react-redux";


const HomePage = () => {
  const cards = useSelector((state) => state.cards.cards, shallowEqual);

  return (
    <>
      <div>
        <CardsContainer cards={cards} />
      </div>

      <div>
        <Cart />
      </div>
    </>
  );
};

export default HomePage;
