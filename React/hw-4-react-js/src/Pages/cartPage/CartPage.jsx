import React from "react";
import CartCardsContainer from "../../Components/CartCardsContainer/CartCardsContainer";

const CartPage = () => {
  return (
    <>
      <div>
        <CartCardsContainer />
      </div>
    </>
  );
};

export default CartPage;
