import React from "react";
import styles from "./CartCardsContainer.module.scss";
import CartItem from "../CartItem/CartItem";
import { useSelector, shallowEqual } from "react-redux";

const CartCardsContainer = () => {
  const cartCards = useSelector((state) => state.cart.cards, shallowEqual);
  return (
    <section className={styles.root}>
      <h1>ТАВАРИ У КОШИКУ</h1>
      <div className={styles.container}>
        {cartCards &&
          cartCards.map(({ name, price, image, id, count }) => (
            <CartItem
              count={count}
              key={id}
              name={name}
              price={price}
              image={image}
              id={id}
            />
          ))}
      </div>
    </section>
  );
};

export default CartCardsContainer;
