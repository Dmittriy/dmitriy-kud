import React from "react";
import styles from "./Modal.module.scss";
import Button from "../Button/Button";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import { setModal } from "../../redux/modal/actionCreators";

const Modal = ({
  active,
  closeButton,
  backgroundColor,
  backgroundColorHeader,
}) => {
  const btnConfirm = useSelector((state) => state.modal.modalAction);
  const titleModal = useSelector((state) => state.modal.title);
  const bodyModal = useSelector((state) => state.modal.body);

  const dispatch = useDispatch();
  const toggleModal = () => {
    dispatch(setModal());
  };

  return (
    <div
      className={
        active
          ? [styles.wrapperModal, styles.active].join(" ")
          : styles.wrapperModal
      }
      onClick={toggleModal}
    >
      <div className={styles.modal} onClick={(e) => e.stopPropagation()}>
        <div
          className={styles.header}
          style={{ background: backgroundColorHeader }}
        >
          <h2 className={styles.title}>{titleModal}</h2>
          {closeButton && (
            <Button
              backgroundColor={"pink"}
              className={styles.closeBtn}
              onClick={toggleModal}
              text="X"
            />
          )}
        </div>
        <div className={styles.conteiner} style={{ backgroundColor }}>
          
          <p>{bodyModal}</p>

          <div className={styles.btnWrapper}>

            {btnConfirm}

            <Button
              backgroundColor={"violet"}
              onClick={toggleModal}
              text="Cancel"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

Modal.propTypes = {
  active: PropTypes.bool.isRequired,
  closeButton: PropTypes.bool,
  backgroundColor: PropTypes.string,
  backgroundColorHeader: PropTypes.string,
};

Modal.defaultProps = {
  closeButton: true,
  active: false,
  backgroundColor: "white",
  backgroundColorHeader: "white",
};

export default Modal;
