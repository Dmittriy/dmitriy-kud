import './App.scss';
import { useState } from 'react';
import Button from "./Components/Button/Button";
import Modal from "./Components/Modal/Modal"

const bodyModalFirst = `
Once you delite this file, it won't be possible to undo this action.
Are you sure you want to delete it ?
`

const bodyModalSecond = `
Once you can save this file.
Are you sure you want to safe it ?
`

function App() {
  const [modalActive, setModalActive] = useState(false);
  const [modalActiveSecond, setModalActiveSecond] = useState(false);

  return (
    <div className="App">
      <Button backgroundColor={'aquamarine'} onClick={() => { setModalActive(true) }} text='Open first modal' />
      <Button backgroundColor={'yellow'} onClick={() => { setModalActiveSecond(true) }} text='Open second modal' />

      <Modal closeButton={false} backgroundColor={'red'} backgroundColorHeader={'#e94040'} active={modalActive}

        actions={
          <>
            <Button backgroundColor={'lightgrey'} onClick={() => alert('Are you schure?')} text='Ok' />
            <Button backgroundColor={'violet'} onClick={() => setModalActive(false)} text='Censel' />
          </>
        }
        setActive={setModalActive} bodyModal={bodyModalFirst} text='Do you want to delete this file?' />

      <Modal
        closeButton={true} backgroundColor={'yellow'} backgroundColorHeader={'blue'} active={modalActiveSecond}

        actions={
          <>
            <Button backgroundColor={'lightgrey'} onClick={() => alert('Are you schure?')} text='Ok' />
            <Button backgroundColor={'violet'} onClick={() => setModalActiveSecond(false)} text='Censel' />
          </>
        }
        setActive={setModalActiveSecond} bodyModal={bodyModalSecond} text='Do you want to save this file?' />
    </div>
  );
}




export default App;
