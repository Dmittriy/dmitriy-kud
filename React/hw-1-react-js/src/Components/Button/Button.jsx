import React from 'react';
import styles from './Button.module.scss';

const Button = ({backgroundColor, text, onClick}) => {
      
  return (
    <>
    <button className={styles.button} style={{backgroundColor}} onClick={onClick} >{text}</button>  
    </>
    
  )
}


export default Button;