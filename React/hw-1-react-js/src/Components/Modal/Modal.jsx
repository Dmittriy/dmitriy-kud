import React from 'react';
import styles from './Modal.module.scss';
import Button from '../Button/Button'

const Modal = ({ active, setActive, closeButton, text, backgroundColor, backgroundColorHeader, bodyModal, actions }) => {

    return (

        <div className={active ? [styles.wrapperModal, styles.active].join(' ') : styles.wrapperModal} onClick={() => setActive(false)} >
            <div className={styles.modal} onClick={(e) => e.stopPropagation()}>
                <div className={styles.header} style={{ background: backgroundColorHeader }}>
                    <h2 className={styles.title}>{text}</h2>
                    {closeButton &&
                        (<Button backgroundColor={'pink'} className={styles.closeBtn}
                            onClick={() => setActive(false)} text='X' />
                        )}
                </div>
                <div className={styles.conteiner} style={{ backgroundColor }}>
                    <p>{bodyModal}</p>

                    <div className={styles.btnWrapper}>
                        {actions}
                    </div>
                </div>
            </div>
        </div>
    )
}


export default Modal;




