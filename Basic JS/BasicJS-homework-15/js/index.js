
// ТЕОРІЯ

// 1. Напишіть, як ви розумієте рекурсію. Навіщо вона використовується на практиці?
//     - рекурсія - це прийом при написанні коду, при котрому використовується возов функції 
//       в середині сомої себе, що спрощуює написання цикла в циклі та робить код трохи меншим




// Практичні завдання




// ------------------------------------------------Загальний розділ--------------------------------


const validNumber = number => !(!number || isNaN(number) || Number.isInteger(number) || !number.trim());

const getNumber = (massage = 'Enter your Number') => {

    let userNumber;

    do {
        userNumber = prompt(massage, userNumber);
    }

    while (!validNumber(userNumber));

    return +userNumber;
}

const userNumber = getNumber('Enter your nomeric');


//-------------------------------------------------------------1 варіант-------------------------------

// const calcFactorial = (a) => {
//     let num = a;

//     if (a <= 1) {
//         return alert(`Факторіал Вашого числа дорівнює = 1`);

//     } else for (let i = a; i >= 2; i--) {
//                num = num * (i - 1);
//               }

//     return alert(`Факторіал Вашого числа дорівнює = ${num}`);
// }

// calcFactorial(userNumber);


// ---------------------------------2 варіант з рекурсією----------------------------------

const calcFactorialV2 = (a) => {

    if (a <= 1) {

        return 1;

    } else return a * calcFactorialV2(a - 1);
}

alert (`Факторіал Вашого числа = ${calcFactorialV2(userNumber)}`);
