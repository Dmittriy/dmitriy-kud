"use strict"

// Теоретичні питання

// 1.Опишіть своїми словами що таке Document Object Model (DOM)

//   це глобальна модель усієї сторінки представлена у вигляді об'єктів , які можна змінювати

// // 2.Яка різниця між властивостями HTML-елементів innerHTML та innerText?
//      innerText - властивість за допомогою якої додається або змінюється текст в середині HTML коду

//      innerHTML - властивість за бопомогою якої додається або змінюється HTML код на сторінці


// 3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

//   формат звернення такий : document.querySelectorAll() універсальний спосіб звернення до всіх елементів сторінки(мабуть самий зручний)

//                          document.querySelector() універсальний спосіб звернення до першого елементів сторінки який вказан в дужках

//   також можливо звернутись за конкретним параметром:
//    - за ID             - document.getElementById()
//    - за className      - document.getElementsByClassName()
//    - за tag            - document.getElementsByTagName()
//     - за name           - document.getElementsByName()


 


// Завдання


// Код для завдань лежить в папці project.

// 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000

// 2.  Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. 
//     Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

// 3. Встановіть в якості контента елемента з ID testParagraph наступний параграф - This is a paragraph

// 4. Отримати елементи <Li>, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

// 5. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.


// 111111111111111111111111111111111111111111111111111111111111111111111

const paragraphs = document.querySelectorAll('p');

paragraphs.forEach(elem => { elem.style.background = '#ff0000' });

// 222222222222222222222222222222222222222222222222222222222222222222222

const elemId = document.querySelector("#optionsList");
console.log(elemId);

const elemIdParent = elemId.parentNode;
console.log(elemIdParent);

let elemIdChild; 
if (elemId.hasChildNodes()) {
    
   elemIdChild = elemId.childNodes
}
const typeName = Array.from(elemIdChild).forEach(elem => console.log(`${elem.nodeType} - ${elem.nodeName} `));

// 33333333333333333333333333333333333333333333333333333333333333333333333

const changeParagraph = document.querySelector("#testParagraph");
changeParagraph.innerHTML = "This is a paragraph"

// 4444444444444444444444444444444444444444444444444444444444444444444444

const elementsMainHeader = document.querySelectorAll(".main-header li");

elementsMainHeader.forEach(elem => {
    elem.classList.add("nav-item")
})

console.log(elementsMainHeader);

// 555555555555555555555555555555555555555555555555555555555555555555555555

const sections = document.querySelectorAll(".section-title")

sections.forEach(elem =>  {
    elem.classList.remove('section-title')
})

console.log(sections);


