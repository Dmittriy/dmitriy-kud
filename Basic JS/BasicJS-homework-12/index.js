"use strict"
/*
## Теоретичні питання

1. Чому для роботи з input не рекомендується використовувати клавіатуру?
мабуть, тому, що це є не надійно, подія input не запускається під час введення з клавіатури та інших дій, які не передбачають зміну значення,
 наприклад натискання клавіш зі стрілками ліворуч, праворуч.

## Завдання

Реалізувати функцію підсвічування клавіш. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

#### Технічні вимоги:
- У файлі `index.html` лежить розмітка для кнопок.
- Кожна кнопка містить назву клавіші на клавіатурі
- Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. 
При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. 
Наприклад за натисканням `Enter` перша кнопка забарвлюється у синій колір. Далі, користувач натискає `S`, 
і кнопка `S` забарвлюється в синій колір, а кнопка `Enter` знову стає чорною. 

#### Література:
-  [Клавіатура: keyup, keydown, keypress](https://learn.javascript.ru/keyboard-events )

// */

const btnsConteiner = document.querySelector('.btn-wrapper');
const btns = document.querySelectorAll('.btn');

btnsConteiner.addEventListener('click', (ev) => {

    if (ev.target === ev.currentTarget) {
        return;
    }
    btns.forEach((el) => {
        el.classList.remove('active');
    })
    if (ev.target) {
        ev.target.classList.add('active');
    };
});

document.body.addEventListener('keydown', (ev) => {
    // console.log(ev.key);
    btns.forEach((el) => {
        el.classList.remove('active');

        if (ev.key.toLowerCase() === el.innerText.toLowerCase()) {
            el.classList.add('active');
        };
    });

    
});
