"use strict"

// ## Завдання

// Написати реалізацію кнопки "Показати пароль". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:
// - У файлі `index.html` лежить розмітка двох полів вводу пароля.
// - Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд. 
//   У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
// - Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
// - Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
// - Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
// - Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – `You are welcome`;
// - Якщо значення не збігаються - вивести під другим полем текст червоного кольору `Потрібно ввести однакові значення`
// - Після натискання на кнопку сторінка не повинна перезавантажуватись
// - Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.


// -----------------------------------------------------------------ПЕРША СПРОБА- ТРОХИ  КРИВА ------------------------------------------------------------
// const passwodrs = document.querySelectorAll('.fas')
// const input = document.querySelectorAll('input')
// const form = document.querySelector('.password-form')

// passwodrs.forEach((elem, index) => {
//     elem.addEventListener('click', (event) => {
//         const eyeSlash = document.querySelector('.fas.fa-eye-slash')
//         const eye = document.querySelector('.fas.fa-eye')

//         if (eyeSlash) {

//             event.target.classList.replace('fa-eye-slash', 'fa-eye')
//             input[index].setAttribute('type', 'text')
//         }

//         if (eye) {
//             event.target.classList.replace('fa-eye', 'fa-eye-slash')
//             input[index].setAttribute('type', 'password')
//         }
//     })

// })

// const form = document.querySelector('.password-form')
// form.addEventListener('submit', (e) => {
//     e.preventDefault()
//     const error = document.querySelector('.js-error')
//     const errorP = document.querySelector('label p')

//     if (errorP) { errorP.remove() }

//     if (input[0].value === input[1].value) {
//         alert('You are welcome')
//     }

//     if (input[0].value !== input[1].value) {
//         error.insertAdjacentHTML('beforeend', '<p style = "color:red">Потрібно ввести однакові значення</p>')
//     }
// })

// -----------------------------------------------------------------ДРУГА СПРОБА - НАЧЕБТО ВСЕ ГУД ) ------------------------------------------------------------

const icon = document.querySelectorAll('.input-wrapper i');

icon.forEach(elem => {
    elem.addEventListener('click', (event) => {
        const input = event.target.parentNode.querySelector('input');
        if (input.type === 'password') {
            input.setAttribute('type', 'text');
            event.target.setAttribute('class', 'fas fa-eye icon-password');

        } else {
            input.setAttribute('type', 'password');
            event.target.setAttribute('class', 'fas fa-eye-slash icon-password');
        }
    })
})

const form = document.querySelector('.password-form');

form.addEventListener('submit', (e) => {
    e.preventDefault();
    const inputFirst = document.querySelector('.js-input-first');
    const inputSecond = document.querySelector('.js-input-second');
    const warning = document.querySelector('.input-wrapper p');

    if (inputFirst.value === inputSecond.value && inputFirst.value !== '' && inputSecond.value !== '') {
        alert('You are welcome');
    }

    if (warning) return;

    if (inputFirst.value !== inputSecond.value) {
        inputSecond.parentNode.insertAdjacentHTML('beforeend', '<p style = "color:red">Потрібно ввести однакові значення</p>');
    }
})

const input = document.querySelectorAll('input');

input.forEach(el => {
    el.addEventListener('focus', (event) => {
        const warning = document.querySelector('.input-wrapper p');
        if (warning) warning.remove();     
    })
})
