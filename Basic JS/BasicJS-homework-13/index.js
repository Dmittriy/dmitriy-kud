"use strict"
/*
## Теоретичні питання

1. Опишіть своїми словами різницю між функціями `setTimeout()` і `setInterval(`)`.
`setTimeout()`-  функція таймеру яка спрацьовує лише один раз
`setInterval(`)`- функція таймеру яка спрацьовує нескінченну кількість разів поки не буде зупинена

2. Що станеться, якщо в функцію `setTimeout()` передати нульову затримку? Чи спрацює вона миттєво і чому?

функція спрацює, без затримки, 0 в неї це значення за замовченням

3. Чому важливо не забувати викликати функцію `clearInterval()`, коли раніше створений цикл запуску вам вже не потрібен?

функція таймеру `setInterval(`)` буде рацьовує нескінченну кількість разів, і буде навантажувати браузер, тому коли вона нам вже непотрібна,
обов'язково потрібно викликати функцію `clearInterval()`


## Завдання

Реалізувати програму, яка циклічно показує різні картинки. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

#### Технічні вимоги:

- У папці `banners` лежить HTML код та папка з картинками.
- При запуску програми на екрані має відображатись перша картинка.
- Через 3 секунди замість неї має бути показано друга картинка.
- Ще через 3 секунди – третя.
- Ще через 3 секунди – четверта.
- Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.
- Після запуску програми десь на екрані має з'явитись кнопка з написом `Припинити`.
- Після натискання на кнопку `Припинити` цикл завершується, на екрані залишається показаною та картинка, яка була там при натисканні кнопки.
- Поруч із кнопкою `Припинити` має бути кнопка `Відновити показ`, при натисканні якої цикл триває з тієї картинки, яка в даний момент показана на екрані.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги. 

#### Необов'язкове завдання підвищеної складності
- При запуску програми на екрані повинен бути таймер з секундами та мілісекундами, що показує, скільки залишилося до показу наступної картинки.
- Робити приховування картинки та показ нової картинки поступовим (анімація fadeOut/fadeIn) протягом 0.5 секунди.

#### Література:
- [setTimeout и setInterval](https://learn.javascript.ru/settimeout-setinterval)
*/


const wrapper = document.querySelector('.images-wrapper');
const btnStop = document.querySelector('.stop');
const btnNext = document.querySelector('.next');
const slides = document.querySelectorAll('.image-to-show')
const sliderNew = [];

for (let i = 0; i < slides.length; i++) {
    sliderNew[i] = slides[i].src;
    slides[i].remove();
}
wrapper.innerHTML = `<img width="400" height="400" src=${sliderNew[0]}>`

let slide = 1;
let slideNext = 0;
const slider = function () {

    wrapper.style.opacity = 0;
    const img = document.createElement("img");
    img.classList.add('image-to-show');

    if (slide <= slides.length - 1) {
        img.src = sliderNew[slide];
    }
    else {
        img.src = sliderNew[slide - slides.length * slideNext];
    }

    wrapper.style.transform = `translateX(-${400 * slide}px)`;

    setTimeout(() => {
        wrapper.appendChild(img);
        wrapper.style.opacity = 1;

    }, 500);

    slide++;
    slideNext = Math.trunc(slide / slides.length);
}

let intervalId = setInterval(slider, 3000)

const btnConteiner = document.querySelector('.btn-conteiner')
setTimeout(() => {
    btnConteiner.style.opacity = 1;
}, 1500)
btnNext.disabled = true;

btnStop.addEventListener('click', () => {
    clearInterval(intervalId)
    btnNext.disabled = false;
})

btnNext.addEventListener('click', () => {
    intervalId = setInterval(slider, 3000)
    btnNext.disabled = true;
})
