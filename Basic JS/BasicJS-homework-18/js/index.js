"use strict"

// Реализовать функцию полного клонирования объекта. Задача должна быть реализована на языке javascript, 
// без использования фреймворков и сторонник библиотек (типа Jquery).
// Технические требования:
// Написать функцию для рекурсивного полного клонирования объекта (без единой передачи по ссылке, 
// внутренняя вложенность свойств объекта может быть достаточно большой).
// Функция должна успешно копировать свойства в виде объектов и массивов на любом уровне вложенности.
// В коде нельзя использовать встроенные механизмы клонирования, такие как функция Object.assign() или спред-оператор.

const arr1 = [1, 2, [3, 4], "name1", { name: 5, age: 6 }]; // якийсь масив

arr1.push = 999; // для перевірки відсутності посилання у 2 масиві

// console.log(arr1);

const arr2 = arr1.map(elem => {

    if (Array.isArray(elem)) {
        elem.map(elem => { elem = elem })
    };

    if (typeof elem === 'object' && elem !== null && !Array.isArray(elem)) {
        return JSON.parse(JSON.stringify(elem));
    }
    elem = elem;

    return elem;
})


arr1[2] = "dsdf" //для перевірки відсутності посилання у 2 масиві


arr1[3] = "1233" //для перевірки відсутності посилання у 2 масиві


arr1[4].name = 23; //для перевірки відсутності посилання у 2 масиві


console.log(arr2); // зкопійований об'єкт



