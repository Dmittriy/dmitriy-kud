// --------------------------------------------блок валідації числа--------------------------------------
const validNumber = number => !(!number || isNaN(number) || Number.isInteger(number) || !number.trim());

const getNumber = (massage = 'Enter your Number') => {

    let userNumber;

    do {
        userNumber = prompt(massage, userNumber);
    }

    while (!validNumber(userNumber));

    return userNumber;
}

const userNumber = getNumber('Enter your nomeric');


// ---------------------------------------------1 варіант----------------------------------------------------------------
// const calcFibonachi = (a) => {
//     let sum1 = 1;
//     let sum2 = 1;
//     let num;
//     let sumNegative2 = -1;

//     if (a >= 0) {

//         if (a == 0) {
//             return 0;
//         }
//           else  for (let i = 3; i <= a; i++) {
//                   num = sum1 + sum2;
//                   sum1 = sum2;
//                   sum2 = num;
//                 }
//         return sum2;
//         }

//         else {
//                 if (a == -1) {
//                    return 1;
//                   } 
//                 else for (let i = -3; i >= a; i--) {
//                     num = sum1 - sumNegative2;
//                     sum1 = sumNegative2;
//                     sumNegative2 = num;
//                }
//                return sumNegative2;
//             }
// }

// alert (`Результат "${userNumber}"-го узагальненого числа Фібоначчі = ${calcFibonachi(userNumber)}`);

// --------------------------------------------------------2 варант з рекурсією 

const calcFibonachi = (a) => {
    if ( a >= 0) {
        if (a <= 1) return a;
     
        else return calcFibonachi(a - 1) + calcFibonachi(a - 2);
        }

    else   return calcFibonachi(-(-a) + 2) - calcFibonachi(-(-a) + 1);
}
 
alert(`Результат "${userNumber}"-го узагальненого числа Фібоначчі = ${calcFibonachi(userNumber)}`);