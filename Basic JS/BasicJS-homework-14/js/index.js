"use strict"

const theme = document.querySelector('#theme')
const btnChenge = document.querySelector(".btn-chenge-style")

if (localStorage.getItem('theme') == 'new') {
    theme.href="./css/styleNew.css";
}

btnChenge.addEventListener('click', () => {

    if (theme.getAttribute('href') === "./css/styleNew.css") {
        theme.href = "./css/styleDefault.css"
        localStorage.setItem('theme', 'default');

    }

    else {
        theme.href = "./css/styleNew.css"
        localStorage.setItem('theme', 'new');

    }

})


// const btnChenge = document.querySelector(".btn-chenge-style")

// btnChenge.addEventListener('click', () => {

//     const table = document.querySelector('.table')
//     table.classList.toggle('table-clicked')

//     const icon = document.querySelectorAll('i')
//     icon.forEach(icon => {
//         icon.classList.toggle('i-clicked')
//     })

//     const btns = document.querySelectorAll('.btn')
//     btns.forEach(btn => {
//         btn.classList.toggle('btn-clicked')
//     })

//     const colomnHeads = document.querySelectorAll('.table-head-colmn')
//     colomnHeads.forEach(head => {
//         head.classList.toggle('table-head-colmn-clicked')
//     })

//     const rowsGrey = document.querySelectorAll('.grey')
//     rowsGrey.forEach(row => {
//         row.classList.toggle('grey-clicked')
//     })

//     const rowsWhite = document.querySelectorAll('.white')
//     rowsWhite.forEach(row => {
//         row.classList.toggle('white-clicked')
//     })


// })


