"use strict"

// Теоретичні питання

// // 1.Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
//    екранування це метод, який використовується у прогруванні для коректного відображення спец символів
//     та кодів за допомогою лівого слеша "\", 
//      наприклад для відображення апострофу буде запис \', та він не сприйметься як закриваюча ковичка


// // 2.Які засоби оголошення функцій ви знаєте?
//    1 - Function declaration
//    2 - Function expression 
//    3 - Named Function expression

// // // 3.Що таке hoisting, як він працює для змінних та функцій?
//          hoisting - це під'йом всіх змінних та функцій вгору видимості області оголошених
//          за допомогою let та  const без їх ініціалзації, це спціальна повідінка JS, 
//          для того щоб на початку знати які перемінні або константи будуть використовуватись у коді


// ------------------Завдання---------------------------------------------------------------------------


// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. 
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
// При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
// Створити метод getAge() який повертатиме скільки користувачеві років.
// Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, 
// з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
// Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.
// Література:
// Дата і час




const createNewUser = (firstName, lastName, birthday) => {
  const newUser = {
    firstName,
    lastName,
    birthday,
    getAge() {
      let userBirthday = new Date(this.birthday.slice(-4), (this.birthday.slice(3, 5) - 1), this.birthday.slice(0, 2));

      const userAge = (Date.now() - userBirthday) / (1000 * 60 * 60 * 24 * 365);

      return Math.trunc(userAge);

    },

    getLogin() {
      return `${this.firstName.toLowerCase().slice(0, 1)}${this.lastName.toLowerCase()}`;
    },

    getPassword() {
      return `${this.firstName.toUpperCase().slice(0, 1)}${this.lastName.toLowerCase()}${this.birthday.slice(-4)}`;

    },

    setFirstName(newFirstName) {
        Object.defineProperty(this, "firstName", { value: newFirstName });

    },

    setLastName(newLastName) {
        Object.defineProperty(this, "lastName", { value: newLastName });

    },
  };

  Object.defineProperties(newUser, {
      firstName: { writable: false },
      lastName: { writable: false },
  },
  );

  return newUser;
};

const user = createNewUser(prompt('What is your Firstname?'), prompt('What is your Lastname?'), prompt('When your Berthday?', "dd.mm.yyyy"));

// user.setFirstName(prompt(`What is your NEW Firstname`));

// user.setLastName(prompt(`What is your NEW Lastname`));

console.log(`Your NEW login = ${user.getLogin()}`);

console.log(`Your Birthday : ${user.birthday}`);

console.log(`Your Password = ${user.getPassword()}`);

console.log(`Your Age = ${user.getAge()}`);

console.log(user);
