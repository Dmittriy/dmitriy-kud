
// ТЕОРІЯ
// 1. Які існують типи даних у Javascript ?
//  існують такі типи даних:
// - строка -  String;
// - число - Number;
// - велике число - BigInt (більше ніж 2^^53);
// - нічого, пусто - null;
// - логічні - Boolean (true або false);
// - невизначений - undefined;
// - символ - Symbol;
// - об'єкт - Object ;


// // 2. У чому різниця між == і ===?
// == - не строге "дорівнює", виконується з неявним перетворенням типів данних та порівння значення , наприклад "3" == 3, так true;
// === - сторге "дорівнює", виконується без неявного перетворення, з порівнянням типу данних та їх вмістом, наприклад "3"==="3", так true,  '3'===3 , ні- false;

// // // 3. Що таке оператор?
//    оператор - це спеціальні символи (вирази) математичні або логічні (й інші), які розташовуються між операндами та виконують математичну або логічну (порівняльну) дію з опрендами
//    3+4 - математичний оператор + та два операнда 3 та 4
//     + - / * % ++ -- **  математичні оператори
//     > < = == === != !== ! && || : - логічні оператори

// // Практичні завдання

let userName;
let userAge;
do {userName = prompt('Enter your Name', userName)
}
while (!userName || !isNaN(userName));

do {userAge = prompt('Enter your Age', userAge)
}
while (!userAge || isNaN(userAge))
{
    if (+userAge < 18) {
        alert(`You are not allowed to visit this website`)
    } else if (+userAge >= 18 && +userAge <= 22) {
        if (confirm(`Are you sure you want to continue?`)) { alert(`Welcome ${userName}`) }
        else { alert(`You are not allowed to visit this website`) }
    }
    else { alert(`Welcome ${userName}`) }
};

console.log(userName);
console.log(userAge);



