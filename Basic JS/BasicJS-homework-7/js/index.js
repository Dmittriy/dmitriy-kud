"use strict"

// Теоретичні питання
// 1.Опишіть своїми словами як працює метод forEach.
// Цей метод перебирає один раз кожний елемент масива та виконує зазначену для нього дію або функцію

// // 2.Як очистити масив?
//    за допомогою методів arr.splice() (де завгодно), arr.pop()(в кінці масиву) або arr.shift(з початку масиву)  
//    маємо можливість видаляти елементи масиву в залежності знаходженні їх у масиві

// // // 3.Як можна перевірити, що та чи інша змінна є масивом?
// за допомогою методу Array.isArray() 



// Завдання
// Реалізувати функцію фільтру масиву за вказаним типом даних. Завдання має бути виконане на чистому Javascript
//  без використання бібліотек типу jQuery або React.
// // Технічні вимоги:
// Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, 
// який міститиме будь-які дані, другий аргумент - тип даних.
// Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих,
//  тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null],
//   і другим аргументом передати 'string', то функція поверне масив [23, null].



const arrUser = ['hello', 'world', 23, '23', null, false];

const typeData = ['string', 23, false, [], undefined, Symbol()];

const userType = prompt(`Enter number of type date`, `0:string or 1:number or 2:Boolean or 3:Object or 4:undefined or 5:Symbol`);

// const filterBy = (arr, type) => {
//     const arrFiltered = [];
//     for (let elem of arr) {
//         if (typeof elem !== typeof typeData[type]) {
//            arrFiltered.push(elem); 
//         }
//     }
//     return console.log(arrFiltered);
// }
// filterBy(arrUser, userType);

const filterBy = arrUser.filter((elem) => typeof elem !== typeof typeData[userType]);

console.log(filterBy)
