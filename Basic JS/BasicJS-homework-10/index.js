"use strict"


// ## Завдання

// Реалізувати перемикання вкладок (таби) на чистому Javascript.

// #### Технічні вимоги:
// - У папці `tabs` лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. 
//    При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
// - Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// - Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. 
//    При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

// #### Література:
// - [Використання data-* атрибутів](https://developer.mozilla.org/ru/docs/Learn/HTML/Howto/Use_data_attributes)


const tabs = document.querySelector('.tabs')
const content = document.querySelectorAll('.tabs-content li')

tabs.addEventListener('click', (e) => {
    if (e.target === e.currentTarget) {
        return;
    }
    const index = e.target.dataset.tabs;

    const active = tabs.querySelector('.active');
    const activeContent = document.querySelector('.js-content-item-active');

    if (active) {
        active.classList.remove('active');
        activeContent.className = "js-content-item-disactive";
    }

    e.target.classList.add('active');
    content[index].className = "js-content-item-active";

});







