// Завдання
// HW4 - Fetch-data

// Теоретичне питання
// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

// AJAX - це певна технологія, що дозволяє підвантажувати ряд даних на веб-сторінці без перевантаження самої сторінки,
//  що, відповідно, зменшує кількість запитів до сервера, а також об’єм підвантажуваних даних.

// Завдання
// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. 
// Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. 
// Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

// Необов'язкове завдання підвищеної складності
// Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження.
//  Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
// Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

fetch(`https://ajax.test-danit.com/api/swapi/films`)
    .then(resp => resp.json())
    .then(data => {
        data.forEach(({ episodeId, name, openingCrawl, characters }) => {
            new StarFilm(episodeId, name, openingCrawl, characters).render();
        });
    })

    .catch(err => console.log(err))

class StarFilm {
    constructor(episodeId, name, openingCrawl, urls) {
        this.episodeId = episodeId;
        this.name = name;
        this.openingCrawl = openingCrawl;
        this.urls = urls;
        this.div = document.createElement('div');
        this.ul = document.createElement('ul');
        this.p = document.createElement('p')
    };

    createElement() {
        this.div.innerHTML = `<h2>Episode : ${this.episodeId} </h2>
                            <h3>Name of movie: ${this.name} </h3>`;

        this.p.innerHTML = `<p> ${this.openingCrawl} </p>`;

        this.urls.forEach(url => {
            fetch(url)
                .then(resp => resp.json())
                .then(({ name }) => {
                    const li = document.createElement('li')
                    li.innerText = `${name} `
                    this.ul.append(li)
                })
        })
        this.ul.innerHTML = '<h3>Characters : </h3>'
        this.div.append(this.ul, this.p)
    }
    render() {
        this.createElement();
        document.body.append(this.div)
    }
}

