// ## Теоретичне питання
// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію `try...catch`.

// `try...catch` доречно використовувати для перехоплення помилок, які можуть виникнути при неуважності написання коду,
//  непередбачуваних вхідних даних від користувачів, неправильній відповіді від сервера або інших причин, при 
//  цьому в нас виводиться помилка, а виконання коду не зупиняється


// Завдання:

// - Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// - На сторінці повинен знаходитись `div` з `id="root"`, куди і потрібно буде додати цей список    
//   (схоже завдання виконувалось в модулі basic).
// - Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність 
//   (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає,
//    в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// - Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.



const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  }
];

const body = document.querySelector('body');
const conteiner = document.createElement("div");
conteiner.id = "root";
conteiner.style = "background-color: yellow;";
body.append(conteiner);


class Item {
  constructor(author, name, price) {
    if (!author) {
      throw new AbsentInformationError("author");
    }
    if (!name) {
      throw new AbsentInformationError("name");
    }
    if (!price) {
      throw new AbsentInformationError("price");
    };
    this.author = author;
    this.name = name;
    this.price = price;
  }

  render() {
    const ul = `<ul>
            <li> Автор книги : ${this.author};</li>
            <li> Назва книги : ${this.name};</li>
            <li> Вартість книги : ${this.price} грн;</li>
          </ul>`
    conteiner.insertAdjacentHTML("beforeend", ul);
  }

}

class AbsentInformationError extends Error {
  constructor(property) {
    super();
    this.name = 'AbsentParametrError';
    this.message = `Absent information: ${property}`
  }
}

books.forEach((e) => {
  try {
    new Item(e.author, e.name, e.price).render();
  } catch (err) {
    if (err.name === 'AbsentParametrError') {
      console.log(err)
    } else {
      throw err;
    }
  }
})


