// Завдання

// Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.


// Технічні вимоги:

// При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. 
// Для цього потрібно надіслати GET запит на наступні дві адреси:

// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts


// Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
// Кожна публікація має бути відображена у вигляді картки (приклад в папці), та включати заголовок, текст,
//  а також ім'я, прізвище та імейл користувача, який її розмістив.
// На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. 
// При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}. 
// Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
// Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
// Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. 
// Це нормально, все так і має працювати.
// Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. 
// При необхідності ви можете додавати також інші класи.


// Необов'язкове завдання підвищеної складності

// Поки з сервера під час відкриття сторінки завантажується інформація, показувати анімацію завантаження. 
// Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
// Додати зверху сторінки кнопку Додати публікацію. При натисканні на кнопку відкривати модальне вікно, 
// в якому користувач зможе ввести заголовок та текст публікації. Після створення публікації дані про неї 
// необхідно надіслати в POST запиті на адресу:  https://ajax.test-danit.com/api/json/posts. 
// Нова публікація має бути додана зверху сторінки (сортування у зворотному хронологічному порядку). 
// Автором можна присвоїти публікації користувача з id: 1.
// Додати функціонал (іконку) для редагування вмісту картки. Після редагування картки для підтвердження змін 
// необхідно надіслати PUT запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}.


axios.get(`https://ajax.test-danit.com/api/json/posts`)
    .then(({ data: posts }) => {

        axios.get(`https://ajax.test-danit.com/api/json/users`)
            .then(({ data: users }) => {
                posts.forEach(({ body, title, userId, id }) => {
                    const { name, email } = users.find(el => el.id === userId);

                    new User(name, email, title, body, id).render();
                })
            })
    })
    .catch(err =>
        console.log(err))




class User {
    constructor(name, email, title, post, postId) {
        this.name = name;
        this.email = email;
        this.title = title;
        this.post = post;
        this.postId = postId
        this.div = document.createElement('div')
        this.btnDel = document.createElement('button')

    }
    createElement() {
        const card = `<span style="font-weight: bold;"> ${this.name}</span>
                 <a class="mail" href="mail:">E-mail:${this.email} </a>
                 <h4 class="title">${this.title}</h4>
                 <p>${this.post}</p>`

        this.div.classList.add('card')
        this.div.insertAdjacentHTML('beforeend', card);
        this.btnDel.classList.add('btn__del')
        this.btnDel.innerText = 'DEL'
        this.div.append(this.btnDel)

        this.btnDel.addEventListener('click', () => {
            axios.delete(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, this.div.remove())
        })
    }

    render() {
        this.createElement();
        const conteiner = document.querySelector('.conteiner')
        conteiner.append(this.div)
    }
}





