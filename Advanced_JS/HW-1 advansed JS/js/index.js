// Теоретичне питання:
// 1.Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// -при прототипному наслідванні новий об'єкт наслідує усі властивості батьківського прототипу, на який він посилається, 
// що зменщує кількість коду та спрощує йгого написання.

// 2.Для чого потрібно викликати super() у конструкторі класу-нащадка?
// - це необхідно в тих випадках коли необхідно розширити метод/функцію прототипу за яким створється новий екземпляр об'єкта
// 
// Завдання

// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). 
// Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {
    constructor(name, age, salary) {
        this._name = prompt(`enter your name`, name);
        this._age = prompt(`enter your age`, age);
        this._salary = prompt(`enter your salary`, salary);
    };

    get name() {
        return this._name;
    };

    set name(newName) {
        this._name = prompt(`enter your name`, newName);
    }

    get age() {
        return this._age;
    };

    set age(newAge) {
        this._age = prompt(`enter your name`, newAge);
    }

    get salary() {
        return this._salary;
    };

    set salary(newSalary) {
        this._salary = prompt(`enter your name`, newSalary);
    }
}

class Programmer extends Employee {
    constructor(lang) {
        super();
        this.lang = lang;
    }

    get salary() {
        return `${super.salary * 3}`;
    };
};

const programmer1 = new Programmer('English');
const programmer2 = new Programmer('Italian');
const programmer3 = new Programmer('Germany');

console.log(programmer1);
console.log(programmer1.salary);

console.log(programmer2);
console.log(programmer2.salary);

console.log(programmer3);
console.log(programmer3.salary);




