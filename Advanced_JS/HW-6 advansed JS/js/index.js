// Теоретичне питання
// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
// - Асинхронність в Javascript це можливість виконання асинхроннного коду в однопотоковому коді,
//  завдяки settimeout, promis та async -await. При цьому запити на сервер відправляються одразу, 
//  а завантажуються та виконуються по черзі після отриманої інформації від сервера. 
//  Це дає можливість виконувати код асинхронно. 

// Завдання
// Написати програму "Я тебе знайду по IP"

// Технічні вимоги:
// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

const btnGetIp = document.querySelector('#btn__get-ip');

btnGetIp.addEventListener('click', async () => {
    try {
        await axios.get('https://api.ipify.org/?format=json')
            .then(async (data) => {
                const ip = data.data.ip;
                const url = `http://ip-api.com/json/${ip}?fields=query,continent,country,city,zip`;

                try {
                    await axios.request(url)
                        .then(data => {
                            const { query: ip, continent, country, city, zip: postIndex } = data.data;

                            const card = `
                                <div class='user__card'>
                                <p> Your continent is ${continent}</p>
                                <p> Your country is ${country}</p>
                                <p> Your city is ${city}</p>
                                <p> Your postIndex is ${postIndex}</p>
                                <p> Your IP is ${ip}</p>
                            `;
                            document.body.insertAdjacentHTML('beforeend', card);

                        });
                } catch (err) {
                    console.log(err);
                }
            });
    } catch (err) {
        console.log(err);
    };
});
